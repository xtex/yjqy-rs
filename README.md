# yjqy-rs — 启业网 API 实现

**yjqy-rs**是[启业网](http://qy.yjzqy.net:9090/list/link_qy.php)的API的Rust实现。启业网是由阳江市第一中学等十多个学校使用的学校信息管理系统。

本项目并非启业网官方项目，与启业网和其使用学校亦无从属关系。

## 贡献

您可以将您对本项目的更改提交至[Git](https://git-scm.com/)仓库的副本后，通过在Codeberg上创建拉取请求或通过`git-send-email`将补丁发送至`xtex <xtexchooser@duck.com>`来请求将更改合并至主仓库。在有一定的贡献后，您可以请求仓库写权限。

任何非直接并入上游的补丁，包括拉取请求和邮件补丁，在合并至上游仓库时，其作者都必须签署[Developer's Certificate of Origin 1.1](https://developercertificate.org/)证明。

## 许可

本项目内容如无特殊声明均采用AGPL-3.0-or-later与CC-BY-NC-SA-4.0双重许可证授权。许可证原文请参考项目附带的`LICENSE`文件。

本项目并非启业网官方项目，与启业网和其使用学校亦无从属关系。

### GNU AGPL v3.0

可选许可证之一为GNU Affero General Public License v3.0，其SPDX许可证标识符为`AGPL-3.0-or-later`。

完整的许可证原文请参考项目附带的`LICENSE.agpl`文件。

> ```
> yjqy-rs Rust implementation for programming interactions with Qiyewang
> Copyright (C) 2024-  xtex and all contributors
> 
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
> 
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
> ```

### CC BY-NC-SA 4.0

可选许可证之二为Creative Commons Attribution Non Commercial Share Alike 4.0 International，其SPDX许可证标识符为`CC-BY-NC-SA-4.0`。

完整的许可证原文请参考项目附带的`LICENSE.ccbyncsa`文件。

>  yjqy-rs     by   xtex and all contributors  is licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International. To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/

