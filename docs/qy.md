# 启业网

启业网是由阳江市第一中学等十多个学校使用的学校信息管理系统。

## 已知资源

启业网主要使用`qy.yjzqy.net`这一域名，大部分服务运行于TCP/9090端口，但TCP/80上亦可访问到一个索引页面。端口扫描未报告其他开放端口。

下方列出的链接均已提交Internet Archive存档。

- TCP/80首页：<http://qy.yjzqy.net/>
- TCP/9090首页：<http://qy.yjzqy.net:9090/>
- 403状态页面：<http://qy.yjzqy.net/wljy/>
- 启业网合作伙伴：<http://qy.yjzqy.net:9090/list/link_qy.php>
- 启业网合作伙伴（空）：<http://qy.yjzqy.net:9090/list/index.php>

## 其他

### ICP备案

`yjzqy.net`域名于2021年11月05日通过ICP备案，备案号为“粤ICP备2021150097号-1”。

### 公安备案

`yjzqy.net`域名于2021年11月25日通过公安备案，网站名称为“骑叶往”，类别为“非交互式”，备案号为“粤公网安备44170202000313号”。

### 域名注册

`yjzqy.net`域名于2019-05-16T09:26:06Z在DNSPod注册，使用DNSPod的免费DNS（`F1G1NS1.DNSPOD.NET`和`F1G1NS2.DNSPOD.NET`），未进行DNSSEC签名。

截至2024年4月，该域名过期时间为2029年05月。

### DNS rrSETs

以下是已知的DNS解析记录：

```
yjzqy.net.              180     IN      SOA     f1g1ns1.dnspod.net. freednsadmin.dnspod.com. 1648659883 3600 180 1209600 180
yjzqy.net.              86400   IN      NS      f1g1ns1.dnspod.net.
yjzqy.net.              86400   IN      NS      f1g1ns2.dnspod.net.
qy.yjzqy.net.           297     IN      A       175.178.41.95
```

未观察到任何AAAA rrSET和DNSSEC相关rrSET。

### 信息查询过滤绕过

参见：<https://blog.xtexx.eu.org/2024/04/13-yjzqy-net-leak-disclosure/>
